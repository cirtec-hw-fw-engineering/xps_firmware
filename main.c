#include <msp430.h> 

volatile long int i;

/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer

	while(1)
	    i++;
	
    #pragma diag_suppress=112
	return 0;
    #pragma diag_default=112
}
